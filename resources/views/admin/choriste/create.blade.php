@extends('layouts.admin.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form method="post" role="form"  action="{{ route('page.store') }}">
                    {{ csrf_field() }}
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="field">
                            <p class="control">
                                <input class="input" type="text" name="pagename" placeholder="Nom de la page">
                            </p>
                        </div>
                    </div>

                    <div class="field">
                        <label class="label">Catégorie de page</label>
                        <p class="control">
                            <span class="select">
                              <select name="id_categ">
                                 @foreach($categs as $categ)
                                      <option value="{{$categ->id}}">{{$categ->name}}</option>
                                  @endforeach
                              </select>
                            </span>
                        </p>
                    </div>




                    <div class="panel-body">
                            <textarea name="mytextarea"  id="mytextarea" class="ckeditor">Hello, World!</textarea>
                            <button type="submit" class="button">Ajout</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
