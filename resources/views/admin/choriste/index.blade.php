@extends('layouts.admin.admin')
@section('content')

    <section class="section">

        <div class="container">

            {{--
                        <h1 class="title">{{ $page->title }}</h1>
                        <div class='body'>
                            <p> {!! $page->content !!}</p>
                        </div>
            --}}

            <a class="button is-primary" href="{{route('choristes.create')}}">Ajouter</a>


            <table class="table">
                <thead>
                <tr>
                    <th>
                        Nom
                    </th>
                    <th>
                        Prenom
                    </th>
                    <th>
                        Pupitre
                    </th>
                    <th>
                        Cotisation
                    </th>
                </tr>
                </thead>
                <tbody>

                @forelse($user as $u)
                    <tr>
                        <th>
                            {{ $u->prenom }}
                        </th>
                        <th>
                            <a href="{{ route('choristes.edit', [$u->id]) }}">
                                {{ $u->nom }}
                            </a>
                        </th>

                        <th>
                            <a href="{{ route('choristes.show', $u->id) }}">
								<?php
								$pupitre =  DB::table('pupitre')->where('id', '=', $u->pupitre_id)->select(DB::raw('libelle'))->get();
								echo $pupitre->pluck('libelle')[0];

								?>
                            </a>
                        </th>
                    </tr>
                @empty
                    <p>Il n'y a pas de page à afficher ! </p>

                @endforelse
                </tbody>
            </table>

        </div>

    </section>

@stop
