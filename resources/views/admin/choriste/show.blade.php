@extends('layouts.admin.admin')
@section('content')

    <section class="section">

        <div class="container">

            <h1 class="title">{{ $page->title }}</h1>
            <div class='body'>
               <p> {!! $page->content !!}</p>
            </div>
        </div>

    </section>

@stop
