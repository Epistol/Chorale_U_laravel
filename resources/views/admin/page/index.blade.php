@extends('layouts.admin.admin')
@section('content')

    <section class="section">

        <div class="container">

            {{--
                        <h1 class="title">{{ $page->title }}</h1>
                        <div class='body'>
                            <p> {!! $page->content !!}</p>
                        </div>
            --}}

            <a class="button is-primary" href="{{route('page.create')}}">Ajouter</a>


            <table class="table">
                <thead>
                <tr>
                    <th>
                        Titre
                    </th>
                    <th>
                        Auteur
                    </th>
                    <th>
                        Date de publication
                    </th>
                    <th>
                        Date de modification
                    </th>
                </tr>
                </thead>
                <tbody>

                @forelse($page as $p)
                    <tr>

                        <th>
                            @php
                                $categ = DB::table('categories')->where('id', $p->categorie_id)->first();;

                            @endphp
                            <a href="{{ route('page.edit', [$p->id]) }}">
                                {{ $p->title }}
                            </a>
                        </th>
                        <th>
                            <a href="{{ route('page.show', $p->id) }}">
                                Voir
                            </a>
                        </th>
                    </tr>
                @empty
                    <p>Il n'y a pas de page à afficher ! </p>

                @endforelse
                </tbody>
            </table>

        </div>

    </section>

@stop
