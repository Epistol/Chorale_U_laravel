@extends('layouts.admin.admin')

@section('content')

        <div class="row">
            <div class="col-md-8 col-md-offset-1">

                <form method="post" role="form"  action="{{ route('page.update', $page->id) }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_method" value="PATCH">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="field">
                                <label class="label">Modification de :  {{ $page->title }}</label>
                                <p class="control">
                                    <input class="input" type="text" name="title" placeholder="Nom de la page" value=" {{ $page->title }}">
                                </p>
                            </div>
                        </div>

                        <div class="panel-body">
                            <textarea name="content"  id="mytextarea" class="ckeditor"> {{ $page->content }}</textarea>
                            <button type="submit" class="button">Modifier</button>

                        </div>

                    </div>

                </form>
            </div>
        </div>


@endsection
