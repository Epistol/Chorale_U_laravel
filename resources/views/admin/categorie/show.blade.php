@extends('layouts.admin.admin')
@section('content')

    <section class="section">

        <div class="container">

            <h1 class="title">{{ $page->name }}</h1>
            <div class='body'>
               <p> {!! $page->description !!}</p>
            </div>
        </div>

    </section>

@stop
