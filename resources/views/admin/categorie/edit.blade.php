@extends('layouts.admin.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <form method="post" role="form"  action="{{ route('ajout_page') }}">
                    {{ csrf_field() }}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="field">
                                <label class="label">Modification de :  {{ $page->title }}</label>
                                <p class="control">
                                    <input class="input" type="text" name="pagename" placeholder="Nom de la page" value=" {{ $page->title }}">
                                </p>
                            </div>
                        </div>

                        <div class="panel-body">
                            <textarea name="mytextarea"  id="mytextarea" class="ckeditor"> {{ $page->content }}</textarea>
                            <button type="submit" class="button">Ajout</button>
                </form>
            </div>
        </div>
    </div>
    </div>
    </div>
@endsection
