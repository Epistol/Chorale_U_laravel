@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 espace_top_logo">
                <div class="panel panel-default">
                    <div class="panel-heading">Inscription pour la saison <?php use Carbon\Carbon;
						$now = Carbon::now();
						echo $now->year ?> /
						<?php
						$now = Carbon::now();
						$now->addYears(1);
						echo $now->year;
						?>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-10 col-md-offset-1">
                            <form class="form-vertical" role="form" method="POST" action="{{ route('register') }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('prenom') ? ' has-error' : '' }} field">
                                    <label for="name" class=" control-label">* Prénom </label>

                                    <input id="prenom" type="text" class="form-control" name="prenom" value="{{ old('prenom') }}" required autofocus>

                                    @if ($errors->has('prenom'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('prenom') }}</strong>
                                    </span>
                                    @endif

                                </div>
                                <div class="form-group{{ $errors->has('nom') ? ' has-error' : '' }} field">
                                    <label for="nom" class=" control-label">* Nom </label>

                                    <input id="nom" type="text" class="form-control" name="nom" value="{{ old('nom') }}" required autofocus>

                                    @if ($errors->has('nom'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('nom') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('adresse') ? ' has-error' : '' }} field">
                                    <label for="nom" class=" control-label">* Adresse :  </label>

                                    <textarea class="textarea" id="adresse" type="text" class="form-control" name="adresse" value="{{ old('adresse') }}" required autofocus> </textarea>
                                    <p class="help ">  (Cette information ne sera pas communiquée à l'extérieur de la chorale.)</p>


                                    @if ($errors->has('adresse'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('adresse') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }} field">
                                    <label for="telephone" class=" control-label">* Telephone </label>
                                    <input id="telephone" type="text" class="form-control" name="telephone" value="{{ old('telephone') }}" required autofocus>
                                    <p class="help ">  (Cette information ne sera pas communiquée à l'extérieur de la chorale.)</p>
                                    @if ($errors->has('telephone'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('telephone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} field">
                                    <label for="email" class=" control-label">* Adresse Email </label>

                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                    <p class="help ">  (Cette information ne sera pas communiquée à l'extérieur de la chorale.)</p>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('regime_administratif') ? ' has-error' : '' }} field">
                                    <label for="regime_administratif" class="control-label">* Régime administratif </label>
                                    <p class="control">
                                    <span class="select">
                                      <select name="regime_administratif">
                                       @foreach($regimes as $regime)
                                              <option value="{{ $regime->id }}">{{ $regime->libelle}}</option>
                                          @endforeach
                                      </select>
                                    </span>
                                    </p>
                                    <p class="help ">  Une seule réponse possible</p>
                                    @if ($errors->has('regime_administratif'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('regime_administratif') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('pupitre') ? ' has-error' : '' }} field">
                                    <label for="pupitre" class=" control-label">* Pupitre </label>

                                    <p class="control">
                                    <span class="select" >
                                      <select name="pupitre">
                                            @foreach($pupitres as $pupitre)
                                              <option value="{{ $pupitre->id }}">{{ $pupitre->libelle}}</option>
                                          @endforeach
                                      </select>
                                    </span>
                                    </p>
                                    <p class="help ">  Une seule réponse possible</p>
                                    @if ($errors->has('pupitre'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('pupitre') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group field">
                                    <p class="control">
                                        <label class="checkbox">
                                            <input type="checkbox" name="dir_pupitre">
                                            J'ai de l'expérience dans la direction de pupitre, et je souhaite en faire bénéficier la Chorale Universitaire.
                                        </label>
                                    </p>
                                    <p class="help">La chorale est toujours à la recherche de chefs de pupitre et adjoints, dans toutes les voix.</p>

                                </div>
                                <div class="form-group field">
                                    <label for="restrictions_alimentaires" class=" control-label">Restrictions alimentaire :  </label>


                                    <textarea class="textarea" id="restrictions_alimentaires" type="text" class="form-control" name="restrictions_alimentaires" value="{{ old('restrictions_alimentaires') }}"  autofocus> </textarea>
                                    <p class="help ">  (Pour les sorties et repas de groupe éventuels)</p>

                                </div>
                                <div class="form-group  field">
                                    <label for="info_sup" class=" control-label">Informations supplémentaire :  </label>


                                        <textarea class="textarea" id="info_sup" type="text" class="form-control" name="info_sup" value="{{ old('info_sup') }}"  autofocus> </textarea>
                                        <p class="help ">  Décrivez ici les compétences dont vous souhaitez faire bénéficier la chorale, vos besoins de prise en charge suite à un handicap, vos absences prévisibles une partie de l'année... ou plus généralement toute information utile à la bonne gestion de la chorale.</p>


                                        @if ($errors->has('info_sup'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('info_sup') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                <div class="form-group{{ $errors->has('reglement') ? ' has-error' : '' }} field">


                                        <p class="control">
                                            <label class="checkbox">
                                                <input type="checkbox" name="reglement">
                                                * J'ai lu et j'accepte le règlement intérieur de la Chorale Universitaire.
                                            </label>
                                        </p>


                                        @if ($errors->has('reglement'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('reglement') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                <div class="form-group field">

                                    <p class="control">
                                        <label class="checkbox">
                                            <input type="checkbox" name="droit_img">
                                            J'accepte que la Chorale Universitaire utilise mon image recueillie dans le cadre des activités, sur tout support interne ou externe à destination de communication ou de promotion.
                                        </label>
                                    </p>


                                </div>
                                <div class="form-group field">
                                    <p class="control">
                                        <label class="checkbox">
                                            <input type="checkbox" name="info_perso">
                                            J'accepte que mes coordonnées personnelles soient diffusées aux autres choristes, en vue de faciliter la communication.
                                        </label>
                                    </p>


                                </div>


                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} field">
                                    <label for="password" class="col-md-4 control-label">Mot de passe</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group field">
                                    <label for="password-confirm" class="col-md-4 control-label">Taper le mot de passe une seconde fois</label>

                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                </div>
                                <div class="form-group field">
                                    <div class="col-md-6 col-md-offset-5">
                                        <button type="submit" class="btn btn-primary">
                                            Inscription
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
