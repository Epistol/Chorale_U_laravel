<a href="#" class="is_hidden" id="top_link">Remonter</a>
<svg id="bigHalfCircle" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="100" viewBox="0 0 100 100" preserveAspectRatio="none">
    <path d="M0 100 C40 0 60 0 100 100 Z"></path>
</svg>
<footer class="footer">
    <div class="container">
        <div class="content has-text-centered">
            <a href="/admin">Admin</a>
            <p>
                <strong>Chorale Universitaire</strong> by <a href="http://jgthms.com">Epistol</a>. The source code is licensed
                <a href="http://opensource.org/licenses/mit-license.php">MIT</a>. The website content
                                       is licensed <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC ANS 4.0</a>.
            </p>
            <p>
                <a class="icon" href="https://github.com/jgthms/bulma">
                    <i class="fa fa-github"></i>
                </a>
            </p>
        </div>
    </div>
</footer>






