<aside class="menu" id="menu">
    <p class="menu-label">
        General
    </p>
    <ul class="menu-list">
        <li><a href="{{route('choristes.index')}}">Choristes</a></li>
        <li><a>Customers</a></li>
    </ul>
    <p class="menu-label">
        Administration
    </p>
    <ul class="menu-list">
        <li  ><a href="{{route('page.index')}}">Pages</a></li>
        <li>
            <a class="{{route('page.create')}}">Ajouter une page</a>
            <ul>
                <li><a href="/admin/categorie/">Catégories de page</a></li>
               {{-- <li><a>Plugins</a></li>
                <li><a>Add a member</a></li>--}}
            </ul>
        </li>
       {{-- <li><a>Invitations</a></li>
        <li><a>Cloud Storage Environment Settings</a></li>
        <li><a>Authentication</a></li>--}}
    </ul>
    <p class="menu-label">
        Transactions
    </p>
    <ul class="menu-list">
        <li><a>Payments</a></li>
        <li><a>Transfers</a></li>
        <li><a>Balance</a></li>
    </ul>
</aside>
