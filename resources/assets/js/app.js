
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: '#app'
});


require('./waypint');



var waypoint = new Waypoint({
    element: document.getElementsByClassName('navbar'),
    handler: function(direction) {
        if (direction === 'up') {
            $( "#top_link" ).removeClass( "go-top" );
            $( "#top_link" ).addClass( "fadeOut" );

        }
        if(direction === 'down'){
            $( "#top_link" ).addClass( "go-top animated" );
        }


    },
    offset: '-30%'
})
