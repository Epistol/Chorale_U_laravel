<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( /**
         * @param \Illuminate\Database\Schema\Blueprint $table
         */
	        'categories', function (Blueprint $table) {
	        $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('slug');
            $table->text('name');
	        $table->longText('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
