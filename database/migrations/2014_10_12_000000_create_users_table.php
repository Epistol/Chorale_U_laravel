<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom');
	        $table->string('prenom');
	        $table->longText('adresse');
	        $table->string('phone');
	        $table->integer('regime_id');
	        $table->integer('pupitre_id');
	        $table->boolean('direction_pupitre')->default(0);
	        $table->longText('alimentaire');
	        $table->longText('infos_supp');
	        $table->boolean('reglement')->default(0);
	        $table->boolean('cotisation')->default(0);
	        $table->boolean('droit_image')->default(0);
	        $table->boolean('infos_perso')->default(0);
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
