<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('pages', function (Blueprint $table) {
	        $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('slug');
            $table->integer('categorie_id')->unsigned();
            $table->text('title');
            $table->longText('content');
            $table->timestamps();
            $table->integer('author');
        });

	    Schema::table('pages', function (Blueprint $table) {
		   $table->foreign('categorie_id')->references('id')->on('categories');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
