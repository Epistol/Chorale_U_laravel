<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $this->call(PupitreTableSeeder::class);
	    $this->call(RegimeTableSeeder::class);
	    $this->call(UserRolesSeeder::class);
    }
}
