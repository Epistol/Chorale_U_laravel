<?php

use Illuminate\Database\Seeder;

class RegimeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('regime_administratif')->insert([
		    'libelle' => "Etudiant qui a choisi la chorale comme UE libre (une note sera donnée à la fin de l'année)",
	    ]);
	    DB::table('regime_administratif')->insert([
		    'libelle' => 'Etudiant libre',
	    ]);
	    DB::table('regime_administratif')->insert([
		    'libelle' => "Demandeur d'emploi",
	    ]);
	    DB::table('regime_administratif')->insert([
		    'libelle' => 'Autre (salarié, retraité ... )',
	    ]);
    }
}
