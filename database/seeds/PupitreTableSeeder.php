<?php

use Illuminate\Database\Seeder;

class PupitreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('pupitre')->insert([
		    'libelle' => 'Soprane',
	    ]);
	    DB::table('pupitre')->insert([
		    'libelle' => 'Alto',
	    ]);
	    DB::table('pupitre')->insert([
		    'libelle' => 'Tenor',
	    ]);
	    DB::table('pupitre')->insert([
		    'libelle' => 'Basse',
	    ]);
	    DB::table('pupitre')->insert([
		    'libelle' => 'Je ne sais pas',
	    ]);

    }
}
