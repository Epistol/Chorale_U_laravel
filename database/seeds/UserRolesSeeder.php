<?php

use HttpOz\Roles\Models\Role;
use Illuminate\Database\Seeder;

class UserRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    Role::create([
		    'name' => 'Admin',
		    'slug' => 'admin',
		    'description' => 'Custodians of the system.', // optional
		    'group' => 'administration' // optional, set as 'default' by default
	    ]);

	    Role::create([
		    'name' => 'Communication',
		    'slug' => 'communication',
		    'group' => 'administration' // optional, set as 'default' by default
	    ]);

	    Role::create([
		    'name' => 'Comptable',
		    'slug' => 'comptable',
		    'description' => "L'argent des abonnés",
		    'group' => 'administration' // optional, set as 'default' by default
		    ]);

	    Role::create([
		    'name' => 'user',
		    'slug' => 'user',
	    ]);

    }
}
