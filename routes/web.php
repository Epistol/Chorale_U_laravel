<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::group(['prefix' => 'admin'], function () {
	Route::group(['middleware' => ['auth']], function () {
		Route::get('/','AdminController@index');
		Route::resource('page', 'PageController');
		Route::resource('categorie', 'CategorieController');
		Route::resource('choristes', 'UserController');

	});

});
/*
Route::get('page/{categorie}/{slug}','PageController@showpublic')->name('public_page');*/
Route::get('categorie/{slug}','CategorieController@showpublic')->name('public_categ');
Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/contact', 'PageController@show_contact');


