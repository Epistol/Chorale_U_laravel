<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use HttpOz\Roles\Models\Role;
use Illuminate\Support\Facades\Auth;

class CheckUserAdmin
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = null)
	{
		$user = User::find(1);

		if (Auth::guard($guard)->check()) {

			if ($user->hasRole('admin')) {
				return $next($request);

			}
			else {
				return redirect()->back();
			}
		}


	}
}
