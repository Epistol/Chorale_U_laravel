<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use HttpOz\Roles\Models\Role;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users as well as their
	| validation and creation. By default this controller uses a trait to
	| provide this functionality without requiring any additional code.
	|
	*/

	use RegistersUsers;

	/**
	 * Where to redirect users after registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/home';

	/**
	 * Create a new controller instance.
	 *
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{

		// TODO : inscription membre
		return Validator::make($data, [
			'prenom' => 'required|max:255',
			'nom' => 'required|max:255',
			'adresse' => 'required',
			'telephone' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'regime_administratif' => 'required',
			'pupitre' => 'required',
			'password' => 'required|min:6|confirmed',
			'reglement' => 'required',
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	protected function create(array $data)
	{

		$userrole = Role::whereSlug('user')->first();


		if(!isset($data['info_sup'])){
			$info_sup = 0;
		}
		else {
			$info_sup = 1;
		}

		if(!isset($data['droit_img'])){
			$droit_img = 0;
		}
		else {
			$droit_img = 1;
		}


		if(!isset($data['info_perso'])){
			$info_perso = 0;
		}
		else {
			$info_perso = 1;
		}
		if(!isset($data['dir_pupitre'])){
			$dir_pupitre = 0;
		}
		else {
			$dir_pupitre = 1;
		}

		if(!isset($data['reglement'])){
			$reglement = 0;
		}
		else {
			$reglement = 1;
		}


		$user =  User::create([
			'nom' => $data['nom'],
			'prenom' => $data['prenom'],
			'password' => bcrypt($data['password']),
			'adresse' => $data['adresse'],
			'phone' => $data['telephone'],
			'regime_id' => $data['regime_administratif'],
			'email' => $data['email'],
			'pupitre_id' => $data['pupitre'],
			'direction_pupitre' => $dir_pupitre,
			'alimentaire' => $data['restrictions_alimentaires'],
			'infos_supp' => $info_sup,
	        'droit_image' => $droit_img,
			'infos_perso' => $info_perso,
			'reglement' => $reglement,


        ]);
		$user->attachRole($userrole);

		return $user;
    }
}
