<?php

namespace App\Http\Controllers;

use App\Categorie;
use App\Http\Requests\StorePagePost;
use Illuminate\Http\Request;
use Illuminate\Session\Store;

class CategorieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	/**
	 * Display a listing of the resource.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$page = Categorie::all();
		if ($request->is('admin/categorie')) {
			return view('admin.categorie.index', compact('page'));
		}



	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('admin.categorie.create');
	}


	public function store(StorePagePost $request)
	{
		$pagename = $request->input('pagename');
		$content = $request->input('mytextarea');
		$slug = preg_replace('/\s+/', '-', $pagename);

		$page = new Categorie;
		$p = $page::firstOrCreate(['name' => $pagename, 'slug' =>$slug, 'description' => $content]);
		$id = $p->id;
		return redirect('admin/categorie/show/'.$id);
	}


	/**
	 * @param $id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function show($id)
	{
		$page = Categorie::findOrFail($id);
		return view('admin.categorie.show', compact('page'));
	}

	public function showpublic($slug)
	{
		$page = Categorie::where('slug','=',$slug)->first();
		return view('show_categ', compact('page'));
	}

	/**
	 * @param $id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function edit($id)
	{
		$page = Categorie::findOrFail($id);
		return view('admin.page.edit', compact('page'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param \App\Categorie            $categorie
	 *
	 * @return \Illuminate\Http\Response
	 * @internal param \App\Page $page
	 */
	public function update(Request $request, Categorie $categorie)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \App\Categorie $categorie
	 *
	 * @return \Illuminate\Http\Response
	 * @internal param \App\Page $page
	 */
	public function destroy(Categorie $categorie)
	{
		//
	}
}
