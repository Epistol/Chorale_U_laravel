<?php

namespace App\Http\Controllers;

use App\Categorie;
use App\Http\Requests\StorePagePost;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PageController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$page = Page::all();
		return view('admin.page.index', compact('page'))->with('phone');

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$categs = DB::table('categories')->distinct()->get();
		return view('admin.page.create', compact('categs'));
	}


	public function store(StorePagePost $request)
	{
		$pagename = $request->input('pagename');
		$content = $request->input('mytextarea');
		$id_categ = $request->input('id_categ');
		$slug = preg_replace('/\s+/', '-', $pagename);


		$page = new Page;
		$p = $page::firstOrCreate(['title' => $pagename, 'slug' =>$slug, 'content' => $content, 'categorie_id' => $id_categ, 'author' => Auth::user()->id]);

		$id = $p->id;

		return redirect()->route('page.show', $page->id)->with('status', 'Page crée');
	}

	public function show_contact(Page $page)
	{
		return view('admin.page.show', ['page' => Page::findOrFail('3')]);
	}

	/**
	 * @param $id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function show($id)
	{
		$page = Page::findOrFail($id);
		return view('admin.page.show', compact('page'));
	}

	public function showpublic($slug)
	{
		/* TODO : Faire la double variable url */
		/*dd($slug);*/
		$page = Page::where('slug','=',$slug)->first();
		return view('show_page', compact('page'));
	}

	/**
	 * @param $id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function edit($id)
	{
		// get the nerd
		$p = Page::find($id);
		// show the edit form and pass the nerd
		return View::make('admin.page.edit')->with('page', $p);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Page  $page
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Page $page)
	{
		$task = Page::findOrFail($page->id);


		$this->validate($request, [
			'title' => 'required',
			'content' => 'required'
		]);

		$input = $request->all();

		$task->fill($input)->save();


		return redirect()->route('page.edit', $page->id)->with('status', 'Page mise à jour');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Page  $page
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Page $page)
	{
		//
	}
}
