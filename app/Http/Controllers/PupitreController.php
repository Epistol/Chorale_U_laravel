<?php

namespace App\Http\Controllers;

use App\Pupitre;
use Illuminate\Http\Request;

class PupitreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pupitre  $pupitre
     * @return \Illuminate\Http\Response
     */
    public function show(Pupitre $pupitre)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pupitre  $pupitre
     * @return \Illuminate\Http\Response
     */
    public function edit(Pupitre $pupitre)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pupitre  $pupitre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pupitre $pupitre)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pupitre  $pupitre
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pupitre $pupitre)
    {
        //
    }
}
