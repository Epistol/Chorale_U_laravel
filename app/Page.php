<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
	public $timestamps = true;
	protected $fillable = ['title', 'content','slug', 'categorie_id','created_at', 'updated_at'];

	public function categ()
	{
		return $this->hasOne('App\Categorie');
	}


}
